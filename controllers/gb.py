from google.appengine.api import users
from google.appengine.api import app_identity

from __init__ import *
from models.models import *


class Guestbook(webapp2.RequestHandler):

    def post(self):
        guestbook_name = self.request.get(
            'guestbook_name', DEFAULT_GUESTBOOK_NAME)

        greeting = Greeting(parent=guestbook_key(guestbook_name))

        userId = 'annonymous'
        if users.get_current_user():
            userId = users.get_current_user().user_id()
            greeting.author = Author(
                identity=userId, email=users.get_current_user().email())

        bucket_name = app_identity.get_default_gcs_bucket_name()
        greeting.content = self.request.get('content')
        greeting.avatar = self.request.get('img')
        file_name = getattr(self.request.POST.get('img'), 'filename')

        greeting.put()

        self.redirect(
            '/?' + urllib.urlencode({'guestbook_name': guestbook_name}))

        query_params = {'guestbook_name': guestbook_name}


