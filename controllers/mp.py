from os import path

import jinja2
from google.appengine.api import users

from __init__ import *
from models.models import *

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(
    path.dirname('.')), extensions=['jinja2.ext.autoescape'], autoescape=True)


class MainPage(webapp2.RequestHandler):

    def _common_work_for_get_post(self):
        logging.debug('MainPage::get()')
        guestbook_name = self.request.get(
            'guestbook_name', DEFAULT_GUESTBOOK_NAME)
        greetings_query = Greeting.query(
            ancestor=guestbook_key(guestbook_name)).order(-Greeting.date)
        greetings = greetings_query.fetch(10)

        user = users.get_current_user()
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user': user,
            'greetings': greetings,
            'guestbook_name': urllib.quote_plus(guestbook_name),
            'url': url,
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('views/index.html')
        self.response.write(template.render(template_values))

    def get(self):
        self._common_work_for_get_post()

    def post(self):
        self._common_work_for_get_post()
